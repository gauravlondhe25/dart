
// Write a program to print all single digits number in reverse order

void main () {
	
	int i = 9;
	while(i>=1) {

		print(i--);
	}
}

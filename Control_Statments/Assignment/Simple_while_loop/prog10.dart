
// Write a program to calculate the factorial of the given number 

void main () {
	
	int i = 1;
	int num = 6;
	int fact = 1;
	while(i<=num) {
		
		fact *= i++;
	}
	print("Factorial of $num is $fact");
}


// Write a program to print the  countdown of the days to submit the assignment

void main () {
	
	int numDays = 7;
	while(numDays >= 1) {
		
		print("$numDays days remaining");		
		numDays--;
	}

	if(numDays == 0)
		print("0 days Assisgnment is overdue");
}

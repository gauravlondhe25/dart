
// Write a dart program, take a number and print whether it is positive or negative

void main () {

	int num = -3;
	
	if(num == 0) 
	
		print("$num is Neutral");
	else if(num > 0)

		print("$num is Positive");
	else 
		print("$num is Negative");
}

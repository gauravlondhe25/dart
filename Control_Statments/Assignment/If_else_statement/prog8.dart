
// Write a dart program to check whether the number is divisible by 3 & 5

void main () {

	int num = 9;
	
	if(num % 3 == 0 && num % 5 == 0) 
	
		print("$num is divisible by both");
	else if(num % 3 == 0) 
	
		print("$num is divisible by 3");
	else if(num % 5 == 0)

		print("$num is divisible by 5");
	else 
		print("$num not divisible by 3 and 5");
}

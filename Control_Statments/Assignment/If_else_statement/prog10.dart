
// Write a dart program to calculate electricity bill of a house based on criteria

void main () {

	int units = 251;
	
	if(units <= 90) 
	
		print(0);
	else if(units <= 180) 
	
		print((units - 90)*6);
	else if(units <= 250) 
	
		print((90*6) + (units - 180)*10);
	else 
		print((90*6) + (70*10) + (units - 250)*15);
}
